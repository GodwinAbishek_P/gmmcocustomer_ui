import { Component, OnInit } from '@angular/core';
declare var $ : any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  currentUrl: string | undefined;
  constructor() { }

  ngOnInit(): void 
  {
    
    $(document).ready(function () {
  
      $(".dropdown-toggle").click(function(){
        if($(this).next(".dropdown-menu").hasClass("show")){
          $(this).next(".dropdown-menu").removeClass("show");
        }
        else{
          $(".dropdown-toggle").next(".dropdown-menu").removeClass("show");
          $(this).next(".dropdown-menu").addClass("show");
        }
      });

    });
  }

}
