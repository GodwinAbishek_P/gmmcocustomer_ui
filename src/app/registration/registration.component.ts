import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ConfirmPasswordValidator } from '../shared/validator';
import { ToastService } from '../services/toastr.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  registerForm!: FormGroup;

  constructor(private auth: AuthService,

    private formBuilder: FormBuilder,
    private toaster: ToastService
  ) {
    // this.registerForm = FormGroup;   
    this.createForm()
  }

  ngOnInit(): void {

  }


  createForm() {
    this.registerForm = this.formBuilder.group({
      ccode: [''],
      panNumber: ["", Validators.required],
      mobile: ["", Validators.required],
      customerName: new FormControl(''),
      companyName: new FormControl(''),


      password: ["", Validators.required],
      passwordConfirm: ["", Validators.required],
      address: [''],
      pincode: [],

      permission: [''],
      status: ['']



    }, {
      validator: ConfirmPasswordValidator("password", "passwordConfirm")
    });
  }




  registration(e: any) {
    console.log(this.registerForm.value)

    // alert(this.registerForm.status)

    let formval = this.registerForm.value;
    formval["isActive"] = true
    formval["createdOn"] = new Date()
    formval["admappOn"] = new Date()
    formval["regOn"] = new Date()
    formval["deactOn"] = new Date()
    formval["permission"] = new Date()
    formval["status"] = "string"
    if (this.registerForm.status == 'VALID') {
      this.auth._Register(formval).subscribe((data: any) => {
        console.log(data)
        if(data.isSuccess == false){
         this.toaster.showError('InValid details')
        }else{
          this.toaster.showSuccess('resgister successfully')
        }


      })
    } else {
      this.toaster.showError('please fill (*) fields to register')
    }
  }


}
