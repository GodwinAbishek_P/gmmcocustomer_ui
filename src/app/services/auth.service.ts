import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl: string;

  constructor(private http:HttpClient) {
    
    this.baseUrl = environment.baseUrl;


  }


  _Login(loginObj:any){
    return this.http.post(this.baseUrl+'CustomerAccount/CustomerLogin',loginObj);
  }
  _Register(regObj:any){
    return this.http.post(this.baseUrl+'CustomerAccount/RegisterUser',regObj);
  }


  _Verify_Customer(verifyObj:any){
    return this.http.post(this.baseUrl+'CustomerAccount/VerifyCustomer',verifyObj)

  }
  _Otp_Send(otpObj:any){
    return this.http.post(this.baseUrl+'CustomerAccount/ResendOTP',otpObj)
  }
}
