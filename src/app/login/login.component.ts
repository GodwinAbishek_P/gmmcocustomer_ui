import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AuthService } from '../services/auth.service';
declare var $: any;
import { ToastService } from '../services/toastr.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;

  constructor(private http: HttpClient, private formBuilder: FormBuilder, private router: Router, private activateroute: ActivatedRoute,
    private auth: AuthService,
    private toaster: ToastService
  ) { }

  ngOnInit(): void {
    this.createForm()

    $(document).ready(function () {
      $(".form-control input").keyup(function () {
        if (($('.form-control').val() != "")) {
          $('.form-control').addClass('typed');
        }
        else {
          $('.form-control').removeClass('typed');
        }

      });
    });

  }

  createForm() {
    this.loginForm = this.formBuilder.group({
      mobileNo: new FormControl(''),
      panNumber: new FormControl(''),
      password: new FormControl(''),
      otp: new FormControl(''),
      loginType: new FormControl('Password'),



    });
  }

  loginFuncForOtp(){
    console.log(this.loginForm.value)
    let loginOBJ=  {
        "userData": {
          "mobileNo": this.loginForm.value.mobileNo,
          "panNumber": this.loginForm.value.panNumber
        },
        "password": this.loginForm.value.Password,
        "loginType":   this.loginForm.value.loginType
      }
      if(loginOBJ.userData.mobileNo != "" && loginOBJ.userData.panNumber != ""){
        this.auth._Login(loginOBJ).subscribe((data:any) => {
          console.log(data)

          if(data.isSuccess == true ){
           this.toaster.showSuccess('sent otp to your registered mobile number')
          }else{
            this.toaster.showError('invalid details')
          }

        })
      }else{
        this.toaster.showError('please enter(*)required fields')
      }

      
  }
  loginFun() {
    console.log(this.loginForm.value)
  let loginOBJ=  {
      "userData": {
        "mobileNo": this.loginForm.value.mobileNo,
        "panNumber": this.loginForm.value.panNumber
      },
      "password": this.loginForm.value.Password,
      "loginType":   this.loginForm.value.loginType
    }
    if(loginOBJ.userData.mobileNo != "" && loginOBJ.userData.panNumber != "" && loginOBJ.password !="" && loginOBJ.password !=null){
      this.auth._Login(loginOBJ).subscribe((data => {
        console.log(data)
      }))
    }else{
      this.toaster.showError('please enter (*)required fields')
    }
   
  }

  otpVerification() {
    // this.router.navigateByUrl('login-verify', { state: { data: this.loginForm.value } });
    let otpObj = {
      "userData": {
        "mobileNo": this.loginForm.value.mobileNo,
        "panNumber": this.loginForm.value.panNumber,
      },
      "otp": this.loginForm.value.otp
    }
  //  alert(otpObj.otp)
    if(otpObj.userData.mobileNo != "" && otpObj.userData.panNumber != "" && otpObj.otp != "" && otpObj.otp != null ){
      this.auth._Verify_Customer(otpObj).subscribe((data: any) => {
        console.log(data)
      })
    }else{
      this.toaster.showError('please enter(*) required field')
    }
    

  }

  sendOtp() {
    let otpObj = {
      "mobileNo": this.loginForm.value.mobileNo,
      "panNumber": this.loginForm.value.panNumber,

    }
    if(otpObj.mobileNo != "" && otpObj.panNumber != ""){
      this.auth._Otp_Send(otpObj).subscribe((data: any) => {
        console.log(data)
      })
    }else{
    alert('please enter all fields')
    }
    
    
  }
}
